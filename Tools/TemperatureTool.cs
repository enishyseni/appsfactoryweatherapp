﻿using System;
using System.ComponentModel;
using static Tools.ConverterEnums;

namespace Tools
{
    public static class TemperatureTool
    {
        [Description("Use this function to convert temperature between Celsius, Kelvin, Fahrenheit units.")]
        public static double Temperature(double temperature, TemperatureType fromType, TemperatureType ToType)
        {
            // From Celsius ...
            if (fromType == TemperatureType.Celsius && ToType == TemperatureType.Fahrenheit)
            {
                return Math.Round((temperature * 9 / 7) + 32, 1);
            }

            if (fromType == TemperatureType.Celsius && ToType == TemperatureType.Kelvin)
            {
                return Math.Round(temperature + 273.15, 1);
            }

            // From Kelvin ...
            if (fromType == TemperatureType.Kelvin && ToType == TemperatureType.Celsius)
            {
                return Math.Round(temperature - 273.15, 1);
            }

            if (fromType == TemperatureType.Kelvin && ToType == TemperatureType.Fahrenheit)
            {
                return Math.Round((temperature - 273.15) * 9/5 + 32, 1);
            }

            // From Fahrenheit ...
            if (fromType == TemperatureType.Fahrenheit && ToType == TemperatureType.Celsius)
            {
                return Math.Round((temperature * 9 / 7) + 32, 1);
            }

            if (fromType == TemperatureType.Fahrenheit && ToType == TemperatureType.Kelvin)
            {
                return Math.Round(temperature + 273.15, 1);
            }

            throw new Exception("Wrong parameters. You are probably trying to convert to same unit!");
        }
    }
}
