using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AFWA.ServiceProvider.DTOs;
using AFWA.ServiceProvider.Contracts;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Security.Claims;
using Tools;
using Microsoft.Extensions.Options;
using AutoMapper;
using AFWA.DataProvider.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;

namespace AfWA.Controllers
{
    [ApiController]
    [Route("api/account")]
    public class AccountController : Controller
    {
        private readonly IdentityConstants _jwtConfig;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        public readonly IConfiguration Configuration;

        public AccountController(SignInManager<User> signInManager,UserManager<User> userManager, IOptions<IdentityConstants> optionsAccessor, IConfiguration configuration)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _jwtConfig = optionsAccessor.Value;
            Configuration = configuration;
        }

        [HttpGet("role/all")]
        public IActionResult GetAllRoles()
        {
            List<string> roleList = new List<string>();
            roleList.Add("Administrator");
            roleList.Add("User");

            return Ok(roleList);
        }


        [HttpPost("login")]       
        public async Task<IActionResult> LogIn([FromBody]LoginDTO model, string returnUrl = null)
        {                    
            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, set lockoutOnFailure: true
            var result = await _signInManager.PasswordSignInAsync(model.UserName, model.Password, false, lockoutOnFailure: false);
            
            if (result.Succeeded)
            {
               
                var user = await _userManager.FindByNameAsync(model.UserName);
            //    user.Claims = _userManager.GetClaimsAsync(user);
                if (!user.Active)
                {
                    return StatusCode(406, new ErrorDTO("Validation", "User not active"));
                }

                var roleList = _userManager.GetRolesAsync(user);
             
                var token = GetJwtSecurityToken(user, roleList.Result.First());

                var userDto = new UserDTO
                {
                    UserName = user.UserName,
                    Email = user.UserName,
                    GivenName = user.GivenName,
                    FamilyName = user.FamilyName,
                    DateCreated = user.DateCreated,
                    Token = new JwtSecurityTokenHandler().WriteToken(token)
                };
               
                return Ok(userDto);
            }
                //if (result.RequiresTwoFactor)
                //{
                //  //  return RedirectToAction(nameof(SendCode), new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                //}
            if (result.IsLockedOut)
            {
                return StatusCode(406, new ErrorDTO("Validation", "User account locked out"));
            }
            return StatusCode(406, new ErrorDTO("Validation", "Invalid login username or password"));             
            
        }

        [HttpGet("logout")]
        public async Task<IActionResult> LogOut()
        {
            await _signInManager.SignOutAsync();

            return Ok();
        }

        [HttpPut("update")]
        public async Task<IActionResult> Update([FromBody]UserDTO model)
        {
          
            var existingUser = await _userManager.FindByNameAsync(model.UserName);

            existingUser.UserName = model.UserName;
            existingUser.Email = model.UserName;
            existingUser.GivenName = model.GivenName;
            existingUser.FamilyName = model.FamilyName;
            existingUser.Active = model.Active;
            
            //Update
            var result = await _userManager.UpdateAsync(existingUser);
            
            if (!result.Succeeded)
            {
                return StatusCode(406, new ErrorDTO(result.Errors.Select(x => x.Code).First(), result.Errors.Select(x => x.Description).First()));
            }

            return NoContent();

        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegisterDTO model)
        {
   
            var user = new User
            {
                UserName = model.UserName,
                Email = model.UserName,
                GivenName = model.GivenName,
                FamilyName = model.FamilyName,
                DateCreated = DateTime.Now,
                EmailConfirmed = true,
                Active = true
            };

          
            //Register
            var result = await _userManager.CreateAsync(user, model.Password);
            
            // Option: enable account confirmation and password reset.
            if (!result.Succeeded)
            {

                return StatusCode(406,new ErrorDTO(result.Errors.Select(x => x.Code).First(), result.Errors.Select(x => x.Description).First()));
            }
          
            var currentUser = await _userManager.FindByNameAsync(user.UserName);

            var roleresult = await _userManager.AddToRoleAsync(currentUser, "User");

               
            //Signin 
            await _signInManager.SignInAsync(user, false);
            
            var token = GetJwtSecurityToken(user, "User");
            
            var userDto = new UserDTO
            {
                UserName = user.UserName,
                Email = user.UserName,
                GivenName = user.GivenName,
                FamilyName = user.FamilyName,            
                DateCreated = user.DateCreated,
                Id = user.Id,
                Token = new JwtSecurityTokenHandler().WriteToken(token)
            };

            //try
            //{
            //    await _emailService.SendEmailAsync(user.Email, "Welcome to AWFA", "Your username and password are ( " + user.UserName + " / " + model.Password + " ). www.bizhawker.com");
            //}
            //finally
            //{

            //}

            //return Ok(userDto);
            return Ok();
        }

        [HttpPost("forgot")]
        public async Task<IActionResult> ForgotPassword([FromBody]ForgotPasswordDTO model)
        {
          
            var user = await _userManager.FindByEmailAsync(model.Email);

            if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
            {
                    // Don't reveal that the user does not exist or is not confirmed.
                    return BadRequest();
            }

            // Send an email with this link                
            var code = await _userManager.GeneratePasswordResetTokenAsync(user);

            //var callbackUrl = Url.Action(nameof(ResetPassword), "Account",
            //        new { UserId = user.Id, Code = code }, protocol: HttpContext.Request.Scheme);


            var callbackUrl = string.Format("{0}://{1}/{2}/{3}/{4}/", HttpContext.Request.Scheme, HttpContext.Request.Host,  "reset", user.Id ,code );

            //  await _emailSender.SendEmailAsync(model.Email, "Reset Password",
            //    $"Please reset your password by clicking here: <a href='{callbackUrl}'>link</a>");

            return NoContent();
        }

        [HttpPost("reset")]
        public IActionResult ResetPassword([FromBody]SetPasswordDTO model)
        {
            var user = _userManager.FindByIdAsync(model.UserId.ToString()).Result;

            IdentityResult result = _userManager.ResetPasswordAsync(user, model.Code, model.ConfirmPassword).Result;

            if (!result.Succeeded)
            {
                return BadRequest();
            }
            return NoContent();
        }

        [HttpPost("tokenverify")]
        public bool TokenVerify(string token)
        {
            return true;
        }



        private JwtSecurityToken GetJwtSecurityToken(User user, string roleName)
        {
            var siteUrl = Encoding.ASCII.GetBytes(Configuration.GetSection("Token:SiteUrl").Value);
            var key = Encoding.ASCII.GetBytes(Configuration.GetSection("Token:Key").Value);
            return new JwtSecurityToken(
                issuer: Encoding.ASCII.GetString(siteUrl),
                audience: Encoding.ASCII.GetString(siteUrl),
                claims: GetTokenClaims(user, roleName),
                expires: DateTime.UtcNow.AddMinutes(45),
                signingCredentials: new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256)
            );
        }

        private static IEnumerable<Claim> GetTokenClaims(User user, string roleName)
        {
           
            return new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                new Claim(JwtRegisteredClaimNames.Sid, roleName)
            };
        }

    }
}