﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AFWA.DataProvider.Entities;
using AFWA.ServiceProvider.Contracts;
using AFWA.ServiceProvider.DTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace AFWA.Controllers
{
    [Authorize]
    [ApiController]    
    [Route("api/weather/")]
    public class WeatherController : Controller
    {
        private readonly IWeatherService<WeatherPredictionDTO> _weatherService;
        private readonly UserManager<User> _userManager;

        public WeatherController(IWeatherService<WeatherPredictionDTO> weatherService, UserManager<User> userManager)
        {
            _weatherService = weatherService;
            _userManager = userManager;
        }

        [HttpPost("forecast")]
        public async Task<ActionResult<WeatherPredictionListDTO>> GetForecast([FromBody]AFWARequest<WeatherPredictionDTO> searchDTO)
        {
            if (!ModelState.IsValid)
            {
                return Forbid();
            }
            searchDTO.UserId = _userManager.Users.First(c => c.UserName == this.User.Claims.First(a => a.Type == ClaimTypes.NameIdentifier).Value).Id;
            return await _weatherService.GetForecast(searchDTO);
        }
        
        [HttpPost("history")]
        public async Task<ActionResult<WeatherPredictionListDTO>> GetHistory([FromBody]AFWARequest<WeatherPredictionDTO> searchDTO)
        {
            if (!ModelState.IsValid)
            {
                return Forbid();
            }
            searchDTO.UserId = _userManager.Users.First(c => c.UserName == this.User.Claims.First(a => a.Type == ClaimTypes.NameIdentifier).Value).Id;
            return await _weatherService.GetHistory(searchDTO);
        }
    }
}
