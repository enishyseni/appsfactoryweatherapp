﻿using System;
using System.Linq;
using AFWA.DataProvider.Entities;
using Microsoft.EntityFrameworkCore;

namespace AFWA.DataProvider
{
    public static class Loc8DbInitializer
    {
        public static void Initialize(AFWADataContext context)
        {
            context.Database.EnsureCreated();

            // Update schema
            context.Database.Migrate();

            // Looks for any Data.
            if (context.WeatherHistory.Any())
            {
                return; // DB has been seeded.
            }

            var samplerecord = new Weather
            {
                Id = Guid.NewGuid(),
                DateCreated = DateTime.Now,
                City = "XYZ",
                DateOfWeather = DateTime.Now,
                TemperatureMax = 2,
                TemperatureMin = 1,
                Humidity = 5,
                WindSpeed = 3                
            };

            context.WeatherHistory.Add(samplerecord);
            context.SaveChanges();

        }
    }
}

