﻿using AFWA.DataProvider.Entities;
using AFWA.DataProvider.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace AFWA.DataProvider.Repositories
{
    class UserRepository : IRepository<User>, IDisposable
    {
        private readonly AFWADataContext dbContext;

        //DbSet usable with any of our entity types
        private DbSet<User> dbSet;

        //constructor taking the database context and getting the appropriately typed data set from it
        public UserRepository(AFWADataContext context)
        {
            dbContext = context;
            dbSet = context.Set<User>();
        }
        
        public async Task<List<User>> GetAllAsync(Expression<Func<User, bool>> where = null, string includeProperties = null, int start = 0, int offset = 0)
        {
            includeProperties = includeProperties ?? string.Empty;
            IQueryable<User> query;
            if (offset > 0)
            {
                query = dbContext.Users.Skip(start).Take(offset);
            }
            else
            {
                query = dbContext.Users;
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }
            if (where != null)
            {
                query = query.Where(where);
            }

            return await query.ToListAsync();
        }

        public virtual void Create(User entity)
        {
            dbSet.Add(entity);
        }

        public virtual User GetById(Guid id)
        {
            return dbSet.Find(id);
        }

        public virtual void Update(User entity)
        {
            throw new Exception("This method is not needed for this solution therefore will not be used!");

        }

        public virtual void Delete(User entity)
        {
            dbContext.Entry(entity).State = EntityState.Deleted;
            dbSet.Remove(entity);
        }
        public virtual void TrueDelete(User entity)
        {            
            throw new Exception("This method is not needed for this solution therefore will not be used!");
        }

        //IDisposable implementation
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dbContext.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public async Task<List<User>> GetWithIncludes(string[] includes)
        {
            IQueryable<User> set = this.dbSet as IQueryable<User>;

            if (includes != null)
            {
                set = includes.Aggregate(set,
                          (current, include) => current.Include(include));
            }
            return await set.ToListAsync();
        }
    }
}