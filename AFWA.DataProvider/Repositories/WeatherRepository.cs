﻿using AFWA.DataProvider.Entities;
using AFWA.DataProvider.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AFWA.DataProvider.Repositories
{
    class WeatherRepository : IRepository<Weather>, IDisposable
    {
        private readonly AFWADataContext dbContext;

        //DbSet usable with any of our entity types
        private DbSet<Weather> dbSet;

        //constructor taking the database context and getting the appropriately typed data set from it
        public WeatherRepository(AFWADataContext context)
        {
            dbContext = context;
            dbSet = context.Set<Weather>();
        }

        public async Task<List<Weather>> GetAllAsync(Expression<Func<Weather, bool>> where = null, string includeProperties = null, int start = 0, int offset = 0)
        {
            includeProperties = includeProperties ?? string.Empty;
            IQueryable<Weather> query;
            if (offset > 0)
            {
                query = dbContext.WeatherHistory.Skip(start).Take(offset);
            }
            else
            {
                query = dbContext.WeatherHistory;
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }
            if (where != null)
            {
                query = query.Where(where);
            }

            return await query.ToListAsync();
        }

        public virtual void Create(Weather entity)
        {
            entity.DateCreated = DateTime.Now;
            dbSet.Add(entity);
        }

        public virtual Weather GetById(Guid id)
        {
            return dbSet.Find(id);
        }

        public virtual void Update(Weather entity)
        {
            dbContext.Entry(entity).State = EntityState.Deleted;
            dbSet.Remove(entity);
        }

        public virtual void Delete(Weather entity)
        {
            dbContext.Entry(entity).State = EntityState.Deleted;
        }
        public virtual void TrueDelete(Weather entity)
        {
            throw new Exception("This method is not needed for this solution therefore will not be used!");
        }

        //IDisposable implementation
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dbContext.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public async Task<List<Weather>> GetWithIncludes(string[] includes)
        {
            IQueryable<Weather> set = this.dbSet as IQueryable<Weather>;

            if (includes != null)
            {
                set = includes.Aggregate(set,
                          (current, include) => current.Include(include));
            }
            return await set.ToListAsync();
        }
    }
}