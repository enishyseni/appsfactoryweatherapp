﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AFWA.DataProvider.Contracts
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task<List<TEntity>> GetAllAsync(Expression<Func<TEntity, bool>> where = null, string includeProperties = null, int start = 0, int offset = 0);
        Task<List<TEntity>> GetWithIncludes(string[] includes);
        TEntity GetById(Guid id);
        void Create(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
        void TrueDelete(TEntity entity);
    }
}
