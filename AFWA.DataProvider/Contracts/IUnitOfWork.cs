﻿using AFWA.DataProvider.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AFWA.DataProvider.Contracts
{
    public interface IUnitOfWork
    {
        IRepository<User> UserRepository { get; }
        IRepository<Weather> WeatherRepository { get; }

        void Commit();
    }
}

