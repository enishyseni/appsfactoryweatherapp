﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using AFWA.DataProvider.Entities;

namespace AFWA.DataProvider
{
    public class AFWADataContext : IdentityDbContext<User, Role, Guid>
    {
        public AFWADataContext(DbContextOptions<AFWADataContext> options) : base(options)
        {

        }

        public DbSet<Weather> WeatherHistory { get; set; }
    }
}
