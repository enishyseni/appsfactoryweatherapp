﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AFWA.DataProvider.Migrations
{
    public partial class addedunitfield : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Unit",
                table: "WeatherHistory",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Unit",
                table: "WeatherHistory");
        }
    }
}
