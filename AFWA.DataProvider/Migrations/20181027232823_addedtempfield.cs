﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AFWA.DataProvider.Migrations
{
    public partial class addedtempfield : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "Temperature",
                table: "WeatherHistory",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Temperature",
                table: "WeatherHistory");
        }
    }
}
