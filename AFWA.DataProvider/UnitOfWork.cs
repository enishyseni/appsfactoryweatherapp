﻿using AFWA.DataProvider.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using AFWA.DataProvider.Entities;
using AFWA.DataProvider.Repositories;

namespace AFWA.DataProvider
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AFWADataContext _afweDataContext;

        private IRepository<User> _userRepository;
        private IRepository<Weather> _weatherRepository;



        public UnitOfWork(AFWADataContext rECBDataContext)
        {
            _afweDataContext = rECBDataContext;
        }

        public IRepository<User> UserRepository
        {
            get
            {
                return _userRepository = _userRepository ?? new UserRepository(_afweDataContext);
            }
        }

        public IRepository<Weather> WeatherRepository
        {
            get
            {
                return _weatherRepository = _weatherRepository ?? new WeatherRepository(_afweDataContext);
            }
        }

        public void Commit()
        {
            _afweDataContext.SaveChanges();
        }
    }
}
