﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;



namespace AFWA.DataProvider
{
    //This class is used only for development. Example: Migrations. We need this approach because this is still just a class library which is in development phase and only becomes part of main project that holds connection string info on application.json
    public class DevDBContextFactory : IDesignTimeDbContextFactory<AFWADataContext>
    {

        public AFWADataContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<AFWADataContext>();
            builder.EnableSensitiveDataLogging(true);
            //builder.UseSqlServer("Data Source=.\\SQLEXPRESS;Initial Catalog=AFWA;Integrated Security=False;User ID=sa;Password=1a2b3c4d5e;Connect Timeout=15;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            builder.UseSqlServer("Server=tcp:enisdb.database.windows.net,1433;Initial Catalog=AFWA;Persist Security Info=False;User ID=enishyseni;Password=ABC1a2b3c@;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");

            return new AFWADataContext(builder.Options);
        }
    }

}




// add-migration initial -Context AFWADataContext -verbose

// update-database -Context AFWADataContext