using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace AFWA.DataProvider.Entities
{
    public class User : IdentityUser<Guid>
    {
        public override Guid Id { get; set; }
        public override string UserName { get; set; }
        public override string Email { get; set; }
        public override bool EmailConfirmed { get; set; }        
        public string GivenName { get; set; }
        public string FamilyName { get; set; }
        public DateTime DateCreated { get; set; }
        public bool Active { get; set; }

        public ICollection<Weather> WeatherHistory { get; set; }
    }
}