﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AFWA.DataProvider.Entities
{
    public class Weather
    {
        public Guid Id { get; set; }
        public string City { get; set; }
        public double Temperature { get; set; }
        public double TemperatureMin { get; set; }
        public double TemperatureMax { get; set; }
        public int Unit { get; set; }
        public double Humidity { get; set; }
        public double WindSpeed { get; set; }
        public DateTime DateOfWeather { get; set; }
        public DateTime DateCreated { get; set; }

        public Guid? UserId { get; set; }
        public User User { get; set; }
    }
}
