﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AFWA.ServiceProvider
{
    class CustomAttributes
    {
        public class MaxStringLengthAttribute : Attribute
        {
            public int MaxLength { get; set; }
            public MaxStringLengthAttribute(int length)
            {
                this.MaxLength = length;
            }
        }
    }
}
