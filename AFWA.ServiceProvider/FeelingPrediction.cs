﻿using System;
using System.Collections.Generic;
using System.Text;
using static Tools.ConverterEnums;

namespace AFWA.ServiceProvider
{
    public static class FeelingPrediction
    {
        public static string Predict(TemperatureType Unit, double AverageTemperature)
        {
            string feelingPrediction = "";
            if (Unit == TemperatureType.Celsius)
            {
                if (AverageTemperature > 35)
                {
                    feelingPrediction = "veryhot";
                }
                if (AverageTemperature > 30 && AverageTemperature < 35)
                {
                    feelingPrediction = "hot";
                }
                if (AverageTemperature > 20 && AverageTemperature < 30)
                {
                    feelingPrediction = "warmer";
                }
                if (AverageTemperature > 10 && AverageTemperature < 20)
                {
                    feelingPrediction = "chill";
                }
                if (AverageTemperature > 5 && AverageTemperature < 10)
                {
                    feelingPrediction = "mildcold";
                }
                if (AverageTemperature > 0 && AverageTemperature < 5)
                {
                    feelingPrediction = "mediumcold";
                }
                if (AverageTemperature > -5 && AverageTemperature < 0)
                {
                    feelingPrediction = "cold";
                }
                if (AverageTemperature < -5)
                {
                    feelingPrediction = "freezing";
                }
            }
            return feelingPrediction;
        }
    }
}
