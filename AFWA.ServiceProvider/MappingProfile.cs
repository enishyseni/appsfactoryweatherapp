﻿using System;
using System.Collections.Generic;
using System.Text;
using AFWA.ServiceProvider.DTOs;
using AFWA.DataProvider.Entities;
using AutoMapper;
using AFWA.ServiceProvider.ProviderModels;
using System.Linq;

namespace AFWA.ServiceProvider
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<DataProvider.Entities.Weather, WeatherPredictionDTO>()
                .ForMember(d => d.TimeOfTheDay, s => s.MapFrom(m => m.DateOfWeather.TimeOfDay.ToString("hh\\:mm")));
            CreateMap<DataProvider.Entities.Weather, DayOfWeatherDTO>();
            CreateMap<User, UserDTO>();

            // Mapping data from OpenWeather JSON to our Weather entity
            CreateMap<WrapperForWeatherObject, DayOfWeatherDTO>()
                .ForMember(d => d.HourlyPredictions, s => s.Ignore()) // try ignoring this
                .ForMember(d => d.DateOfWeather, s => s.MapFrom(m => m.DateOfWeather))
                .ForMember(d => d.DateOfWeatherText, s => s.MapFrom(m => m.DateOfWeather.Date.ToLongDateString().Replace(m.DateOfWeather.Date.DayOfWeek.ToString() + ", ", "")))
                .ForMember(d => d.DayOfWeather, s => s.MapFrom(m => m.DayOfWeather))
                .ForMember(d => d.Temperature, s => s.MapFrom(m => Math.Round(m.HourlyPredictions.Select(t => t.Temperature).Average(), 1)))
                .ForMember(d => d.TemperatureMax, s => s.MapFrom(m => m.HourlyPredictions.Select(t => t.TemperatureMax).Max()))
                .ForMember(d => d.TemperatureMin, s => s.MapFrom(m => m.HourlyPredictions.Select(t => t.TemperatureMin).Min()))
                .ForMember(d => d.FeelingPrediction, s => s.MapFrom(m => m.FeelingPrediction));

            CreateMap<List, DataProvider.Entities.Weather>()
                .ForMember(d => d.Temperature, s => s.MapFrom(m => m.main.temp))
                .ForMember(d => d.TemperatureMax, s => s.MapFrom(m => m.main.tempmax))
                .ForMember(d => d.TemperatureMin, s => s.MapFrom(m => m.main.tempmin))
                .ForMember(d => d.Humidity, s => s.MapFrom(m => m.main.humidity))
                .ForMember(d => d.WindSpeed, s => s.MapFrom(m => m.wind.speed))
                .ForMember(d => d.DateOfWeather, s => s.MapFrom(m => DateTime.Parse(m.dttxt)));            
        }
    }
}
