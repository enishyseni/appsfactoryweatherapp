﻿using AFWA.DataProvider.Contracts;
using AFWA.DataProvider.Entities;
using AFWA.ServiceProvider.Contracts;
using AFWA.ServiceProvider.DTOs;
using AFWA.ServiceProvider.ProviderModels;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using static Tools.ConverterEnums;

namespace AFWA.ServiceProvider.Services
{
    public class WeatherService : IWeatherService<object>
    {
        public readonly IConfiguration _appConfiguration;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _db;
        private string _providerEndpoint;
        private string _providerKey;


        public WeatherService(IConfiguration appConfiguration, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _appConfiguration = appConfiguration;
            _db = unitOfWork;
            _mapper = mapper;
            _providerEndpoint = _appConfiguration.GetSection("WeatherProviders:OpenWeather:Endpoint").Value;
            _providerKey = _appConfiguration.GetSection("WeatherProviders:OpenWeather:Key").Value;
        }

        [Description("This method returns a list of objects containing dayofweather and all weather predictions for that day.")]
        public async Task<WeatherPredictionListDTO> GetForecast(AFWARequest<object> search)
        {
            List<WrapperForWeatherObject> weatherForecast = new List<WrapperForWeatherObject>();
            WeatherPredictionListDTO weatherNowListDTO = new WeatherPredictionListDTO();
            weatherNowListDTO.DaysPredictions = new List<DayOfWeatherDTO>();
            weatherNowListDTO.Unit = search.Unit;
            string endpointToBeCalled = _providerEndpoint + search.City + _providerKey;
            HttpClient _httpClient = new HttpClient();
            HttpResponseMessage response = await _httpClient.GetAsync(endpointToBeCalled);

            // Since we are dealing with external service API, in order to ensure stability 
            // on our part of the plaftorm we will be wrapping the request with Try/Catcth block.
            try
            {
                if (response.IsSuccessStatusCode)
                {
                    var res = await response.Content.ReadAsStringAsync();
                    var cleanedRes = res.Replace("_", "");
                    // This method does not correctly map temp, tempmin, tempmax fields, it makes them get the same 
                    // value for some reason, even after removing all underscore characters from JSON string!
                    var openWeatherForecast = JsonConvert.DeserializeObject<OpenWeatherForecast>(cleanedRes);
                    int index = 0;

                    // This part of code may look slow but since we are working with small amount of data that to this point 
                    // is already in memory and there's no other way to do it faster, I think it does the job well for this case.
                    foreach (var wItem in openWeatherForecast.list)
                    {
                        var currentItemWeatherDate = DateTime.Parse(wItem.dttxt);
                        if (currentItemWeatherDate.Date > DateTime.Now.Date)
                        {
                            if (weatherForecast.Find(c => c.DateOfWeather.Date == currentItemWeatherDate.Date) == null)
                            {
                                WrapperForWeatherObject dayWrapper = new WrapperForWeatherObject();
                                dayWrapper.HourlyPredictions = new List<DataProvider.Entities.Weather>();

                                dayWrapper.DateOfWeather = currentItemWeatherDate;
                                dayWrapper.DayOfWeather = currentItemWeatherDate.DayOfWeek.ToString();

                                foreach (var dItem in openWeatherForecast.list)
                                {
                                    DataProvider.Entities.Weather hPred = Mapper.Map<DataProvider.Entities.Weather>(dItem);
                                    if (dayWrapper.HourlyPredictions.Find(c => c.DateOfWeather.TimeOfDay == hPred.DateOfWeather.TimeOfDay) == null)
                                    {                                        
                                        if (dayWrapper.DateOfWeather.Date == hPred.DateOfWeather.Date)
                                        {
                                            if (search.Unit == TemperatureType.Celsius)
                                            {
                                                hPred.Temperature = Tools.TemperatureTool.Temperature(hPred.Temperature, TemperatureType.Kelvin, TemperatureType.Celsius);
                                                hPred.TemperatureMax = Tools.TemperatureTool.Temperature(hPred.TemperatureMax, TemperatureType.Kelvin, TemperatureType.Celsius);
                                                hPred.TemperatureMin = Tools.TemperatureTool.Temperature(hPred.TemperatureMin, TemperatureType.Kelvin, TemperatureType.Celsius);
                                            }
                                            if (search.Unit == TemperatureType.Fahrenheit)
                                            {
                                                hPred.Temperature = Tools.TemperatureTool.Temperature(hPred.Temperature, TemperatureType.Kelvin, TemperatureType.Fahrenheit);
                                                hPred.TemperatureMax = Tools.TemperatureTool.Temperature(hPred.TemperatureMax, TemperatureType.Kelvin, TemperatureType.Fahrenheit);
                                                hPred.TemperatureMin = Tools.TemperatureTool.Temperature(hPred.TemperatureMin, TemperatureType.Kelvin, TemperatureType.Fahrenheit);
                                            }
                                            dayWrapper.HourlyPredictions.Add(hPred);
                                        }
                                    }
                                }

                                double avgTemp = Math.Round(dayWrapper.HourlyPredictions.Select(t => Convert.ToDouble(t.TemperatureMax)).Average(), 1);
                                dayWrapper.FeelingPrediction = FeelingPrediction.Predict(search.Unit, avgTemp);

                                weatherForecast.Add(dayWrapper);
                                if (weatherForecast.Find(c => c.DateOfWeather.Date == currentItemWeatherDate.Date) == null)
                                {
                                    index++;
                                    if (index == 5) // We will get forecast only for next 5 days even though the service already provides only 5 days if you count today's forecast.
                                        break;
                                }
                            }
                        }
                    }

                    // Remove old predictions so that they can be replaced with more up to date 
                    // predictions if you check the weather many times per day
                    foreach (var newWeatherItem in weatherForecast)
                    {
                        var existingWeatherItem = await _db.WeatherRepository.GetAllAsync(c => c.DateOfWeather.Date == newWeatherItem.DateOfWeather.Date && c.UserId == search.UserId && c.City == search.City, null, 0, 0);
                        foreach (var toBeDeleted in existingWeatherItem)
                        {
                            _db.WeatherRepository.Delete(toBeDeleted);
                        }
                    }

                    // Save new result from API to our database
                    foreach (var newWeatherItem in weatherForecast)
                    {
                        DayOfWeatherDTO tempObj = new DayOfWeatherDTO();
                        tempObj = Mapper.Map<DayOfWeatherDTO>(newWeatherItem);
                        tempObj.HourlyPredictions = new List<WeatherPredictionDTO>();

                        foreach (var wItem in newWeatherItem.HourlyPredictions)
                        {
                            tempObj.HourlyPredictions.Add(Mapper.Map<WeatherPredictionDTO>(wItem));
                            wItem.Unit = (int)search.Unit;
                            wItem.City = search.City;
                            wItem.UserId = search.UserId;
                            _db.WeatherRepository.Create(wItem);
                        }
                        weatherNowListDTO.DaysPredictions.Add(_mapper.Map<DayOfWeatherDTO>(tempObj));
                    }

                    _db.Commit();                                        
                    
                    // Making advanced calculations that we may need in fronted to change the layout based on different levels of temperature
                    // For example, if temperatures are very low we can display a winter image on background of the page to show how the weather might feel next 5 days
                    weatherNowListDTO.AdvancedCalculations();
                }
                else
                {
                    throw new Exception("Service unavailable at the moment!");
                }
            }
            catch(Exception e)
            {
                throw new Exception("Service unavailable at the moment!");
            }

            return weatherNowListDTO;
        }

        [Description("Use this method to get weather objects of past searches. The data returned by this method is leftover history and will not be modified by future actions.")]
        public async Task<WeatherPredictionListDTO> GetHistory(AFWARequest<object> search)
        {
            var weatherForecastDBResult = await _db.WeatherRepository.GetAllAsync(c => c.UserId == search.UserId, null, search.Start, search.Offset);
            weatherForecastDBResult = weatherForecastDBResult.OrderByDescending(c => c.DateOfWeather).ToList();
            if (weatherForecastDBResult.Count() > 0)
            {
                WeatherPredictionListDTO weatherNowListDTO = new WeatherPredictionListDTO();
                weatherNowListDTO.Unit = search.Unit;

                List<WrapperForWeatherObject> weatherForecast = new List<WrapperForWeatherObject>();

                foreach (var wItem in weatherForecastDBResult)
                {
                    var currentItemWeatherDate = wItem.DateOfWeather;
                    //if (currentItemWeatherDate.Date > DateTime.Now.Date)
                    //{
                        if (weatherForecast.Find(c => c.DateOfWeather.Date == currentItemWeatherDate.Date) == null)
                        {
                            WrapperForWeatherObject dayWrapper = new WrapperForWeatherObject();
                            dayWrapper.HourlyPredictions = new List<DataProvider.Entities.Weather>();

                            dayWrapper.DateOfWeather = currentItemWeatherDate;
                            dayWrapper.DayOfWeather = currentItemWeatherDate.DayOfWeek.ToString();

                            foreach (var dItem in weatherForecastDBResult)
                            {
                                DataProvider.Entities.Weather hPred = Mapper.Map<DataProvider.Entities.Weather>(dItem);
                                if (dayWrapper.HourlyPredictions.Find(c => c.DateOfWeather.TimeOfDay == hPred.DateOfWeather.TimeOfDay) == null)
                                {
                                    if (dayWrapper.DateOfWeather.Date == hPred.DateOfWeather.Date)
                                    {
                                        dayWrapper.HourlyPredictions.Add(hPred);
                                    }
                                }
                            }

                            double avgTemp = Math.Round(dayWrapper.HourlyPredictions.Select(t => Convert.ToDouble(t.Temperature)).Average(), 1);
                            dayWrapper.FeelingPrediction = FeelingPrediction.Predict(search.Unit, avgTemp);

                            weatherForecast.Add(dayWrapper);
                        }
                    //}
                }

                weatherNowListDTO.DaysPredictions = new List<DayOfWeatherDTO>();
                foreach (var wItem in weatherForecast)
                {
                    DayOfWeatherDTO tempObj = new DayOfWeatherDTO();
                    tempObj = Mapper.Map<DayOfWeatherDTO>(wItem);
                    tempObj.HourlyPredictions = new List<WeatherPredictionDTO>();

                    foreach (var hItem in wItem.HourlyPredictions)
                    {
                        tempObj.HourlyPredictions.Add(Mapper.Map<WeatherPredictionDTO>(hItem));
                    }
                    weatherNowListDTO.DaysPredictions.Add(_mapper.Map<DayOfWeatherDTO>(tempObj));
                }
                weatherNowListDTO.AdvancedCalculations();

                return weatherNowListDTO;
            }
            else
            {
                return new WeatherPredictionListDTO();
            }
        }
    }
}
