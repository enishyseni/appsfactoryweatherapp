﻿using AFWA.ServiceProvider.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AFWA.ServiceProvider.Contracts
{
    public interface IWeatherService<Principal> where Principal : class // Generics aren't needed for the moment unless we start using this plaform for processing data from the clients, then we can use real objects in the Principal subject
    {
        Task<WeatherPredictionListDTO> GetForecast(AFWARequest<Principal> searchDTO);
        Task<WeatherPredictionListDTO> GetHistory(AFWARequest<Principal> searchDTO);
    }
}
