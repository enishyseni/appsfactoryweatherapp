﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AFWA.ServiceProvider.DTOs
{
    public class SetPasswordDTO
    {
        public Guid UserId { get; set; }
        public string Code { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
