﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Tools.ConverterEnums;

namespace AFWA.ServiceProvider.DTOs
{
    public class WeatherPredictionListDTO
    {
        public List<DayOfWeatherDTO> DaysPredictions { get; set; }
        public TemperatureType Unit { get; set; }
        public double AverageTemperature { get; set; }
        public string WeekFeelingPrediction { get; set; }
        public int Count { get; set; }

        public void AdvancedCalculations()
        {
            this.AverageTemperature = Math.Round(this.DaysPredictions.SelectMany(t => t.HourlyPredictions).Select(tt => Convert.ToDouble(tt.Temperature)).Average(), 1);
            this.WeekFeelingPrediction = FeelingPrediction.Predict(this.Unit, this.AverageTemperature);
            this.Count = this.DaysPredictions.Count();
        }
    }
}
