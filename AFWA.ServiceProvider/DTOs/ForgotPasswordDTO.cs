﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AFWA.ServiceProvider.DTOs
{
    public class ForgotPasswordDTO
    {
        public string Email { get; set; }
    }
}
