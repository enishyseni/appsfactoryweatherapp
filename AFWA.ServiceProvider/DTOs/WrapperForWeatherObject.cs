﻿using AFWA.DataProvider.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AFWA.ServiceProvider.DTOs
{
    class WrapperForWeatherObject
    {
        public string DayOfWeather { get; set; }
        public DateTime DateOfWeather { get; set; }
        public string FeelingPrediction { get; set; }
        public List<Weather> HourlyPredictions { get; set; }
    }
}
