﻿using System;
using System.Collections.Generic;
using System.Text;
using static Tools.ConverterEnums;

namespace AFWA.ServiceProvider.DTOs
{
    public class DayOfWeatherDTO
    {
        public string City { get; set; }
        public string DayOfWeather { get; set; }
        public DateTime DateOfWeather { get; set; }
        public string DateOfWeatherText { get; set; }
        public string FeelingPrediction { get; set; }
        public double Temperature { get; set; }
        public double TemperatureMax { get; set; }
        public double TemperatureMin { get; set; }
        public TemperatureType Unit { get; set; }
        public List<WeatherPredictionDTO> HourlyPredictions { get; set; }
    }
}
