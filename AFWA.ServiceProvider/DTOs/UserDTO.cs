﻿using System;
using System.Collections.Generic;

namespace AFWA.ServiceProvider.DTOs
{
    public class UserDTO
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string GivenName { get; set; }
        public string FamilyName { get; set; }
        public string Token { get; set; }
        public bool Active { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
