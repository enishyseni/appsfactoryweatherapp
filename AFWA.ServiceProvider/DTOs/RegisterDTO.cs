﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AFWA.ServiceProvider.DTOs
{
    public class RegisterDTO
    {
        public string UserName { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }

        public string GivenName { get; set; }

        public string FamilyName { get; set; }
    }
}
