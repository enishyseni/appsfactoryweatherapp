﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AFWA.ServiceProvider.DTOs
{
    public class ErrorDTO
    {
        public ErrorDTO() { }
        public ErrorDTO(string error, string description) {
            Error = error;
            Description = description;
        }

        public string Error { get; set; }
        public string Description { get; set; }
    }
}
