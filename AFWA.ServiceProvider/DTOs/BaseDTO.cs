﻿using System;
using System.Collections.Generic;
using System.Text;
using static Tools.ConverterEnums;

namespace AFWA.ServiceProvider.DTOs
{
    public class BaseDTO
    {
        public string TimeOfTheDay { get; set; }
        public string Temperature { get; set; }
        public string TemperatureMin { get; set; }
        public string TemperatureMax { get; set; }
        public string Humidity { get; set; }
        public string WindSpeed { get; set; }
    }
}
