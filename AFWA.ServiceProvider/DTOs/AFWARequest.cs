﻿using System;
using System.Collections.Generic;
using System.Text;
using static AFWA.ServiceProvider.CustomAttributes;
using static Tools.ConverterEnums;

namespace AFWA.ServiceProvider.DTOs
{
    public class AFWARequest<Principal> where Principal : class
    {
        public AFWARequest()
        {
            // By default we are going to return 5 days
            this.Start = 0;
            this.Offset = 0;
        }

        // I left this for future implementation of CRUD requests in case we have to extend this project to recieve data from different frontend model forms
        public Principal Subject { get; }

        [MaxStringLength(50)]
        public string City { get; set; }
        public TemperatureType Unit { get; set; }

        // In case we decide to use pagination or lazy loading
        public int Start { get; set; }
        public int Offset { get; set; }

        public Guid UserId { get; set; }
    }
}
