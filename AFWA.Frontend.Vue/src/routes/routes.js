import Login from '../components//Login.vue'
import Register from '../components/Register.vue'
import Forecast from '../components/Forecast.vue'
import MyForecastHistory from '../components/MyForecastHistory.vue'
import NotFound from '../components//NotFoundPage.vue'

const routes = [
    {
        path: '/',
        redirect: '/login',
    },
    {
        path: '/login',
        name: 'Login',
        component: Login,
        meta: {
            guest: true
        }
    },
    {
        path: '/register',
        name: 'Register',
        component: Register,
        meta: {
            guest: true
        }
    },
    {
        path: '/forecast',
        name: 'Forecast',
        component: Forecast,
        meta: {
            guest: false
        }
    },
    {
        path: '/myforecasthistory',
        name: 'MyForecastHistory',
        component: MyForecastHistory,
        meta: {
            guest: false
        }
    },
    { path: '*', component: NotFound }
]

export default routes