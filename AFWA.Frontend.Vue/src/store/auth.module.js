import ApiService from '@/services/api.service'
import JwtService from '@/services/jwt.service'
import { LOGIN, LOGOUT, REGISTER, CHECK_AUTH } from './actions.type'
import { SET_AUTH, PURGE_AUTH, SET_ERROR } from './mutations.type'
import router from '../router'

const state = {
  errors: null,
  user: {},
  isAuthenticated: !!JwtService.getToken()
}

const getters = {
  currentUser (state) {
    return state.user
  },
  isAuthenticated (state) {
    return state.isAuthenticated
  }
}

const actions = {
  [LOGIN] (context, credentials) {
    return new Promise((resolve) => {
      ApiService
        .post('api/account/login', {username: credentials.username, password: credentials.password})
        .then(({data}) => {
          context.commit(SET_AUTH, data.Token)
          resolve(data)
          router.push({ name: 'Forecast' })
        })
        .catch(({response}) => {
          context.commit(SET_ERROR, response.data.errors)
        })
    })
  },
  [LOGOUT] (context) {
    context.commit(PURGE_AUTH)
  },
  [REGISTER] (context, model) {
    return new Promise((resolve, reject) => {
      ApiService
        .post('api/account/register', {username: model.username, email: model.email, password: model.password})
        .then(({data}) => {
          context.commit(LOGIN, data)
          resolve(data)
        })
        .catch(({response}) => {
          context.commit(SET_ERROR, response.data.errors)
        })
    })
  },
  [CHECK_AUTH] (context) {
    if (JwtService.getToken()) {
      ApiService.setHeader()
      ApiService
        .post('api/account/tokenverify', {token: JwtService.getToken()})
        .then(({data}) => {
          //context.commit(SET_AUTH, data.token)
          if(data != true){
            alert('Your login session has expired. Please login again to proceed.')
            context.commit(PURGE_AUTH)            
          }
        })
        .catch(({response}) => {
          context.commit(SET_ERROR, response.data.errors)
          alert('Your login session has expired. Please login again to proceed.')
          context.commit(PURGE_AUTH)            
        })
    } else {
      context.commit(PURGE_AUTH)
      if(router.history.pending.name != 'Login' && router.history.pending.name != 'Register')
      {
        console.log('Please login')
        router.push({ name: 'Login' })
      }
    }
  }
}

const mutations = {
  [SET_ERROR] (state, error) {
    state.errors = error
  },
  [SET_AUTH] (state, token) {
    state.isAuthenticated = true
    state.user = {}
    state.errors = {}
    JwtService.saveToken(token)
  },
  [PURGE_AUTH] (state) {
    state.isAuthenticated = false
    state.user = {}
    state.errors = {}
    JwtService.destroyToken()
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}