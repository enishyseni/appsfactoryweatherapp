import Vue from 'vue'
import App from './App.vue'

import './assets/main.css'
import VueMouseParallax from 'vue-mouse-parallax'

import Forecast from "./components/Forecast.vue";
import MyForecastHistory from "./components/MyForecastHistory.vue";
import Register from "./components/Register.vue";
import Login from "./components/Login.vue";
import NotFoundPage from "./components/NotFoundPage.vue";

import ApiService from '@/services/api.service'
import ErrorFilter from '@/services/error.filter'

import router from './router'
import store from '@/store'

Vue.use(Forecast)
Vue.use(MyForecastHistory)
Vue.use(Register)
Vue.use(Login)
Vue.use(NotFoundPage)
Vue.use(VueMouseParallax)

Vue.filter('error', ErrorFilter)

ApiService.init()

Vue.config.productionTip = true

new Vue({
  render: h => h(App),
  store,
  router,  
  template: '<App/>',
  components: { App }

}).$mount('#app')
