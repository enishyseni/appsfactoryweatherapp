import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import JwtService from '@/services/jwt.service'
import { API_URL } from '@/services/config'

//import { PURGE_AUTH } from '@/store/mutations.type'
//import store from "@/store";

const ApiService = {
  init () {
    Vue.use(VueAxios, axios)
    Vue.axios.defaults.baseURL = API_URL
  },

  setHeader () {
    Vue.axios.defaults.headers.common['Authorization'] = `Bearer ${JwtService.getToken()}`
  },

  query (resource, params = '') {
    return Vue.axios
      .get(`${resource}/`, params)
      .catch((error) => {
        throw new Error(`[RWV] ApiService ${error}`)
      })
  },

  get (resource, params = '') {
    return Vue.axios
      .get(`${resource}/${params}`)
      .catch((error) => {
        throw new Error(`[RWV] ApiService ${error}`)
      })
  },

  post (resource, params) {
    return Vue.axios.post(`${resource}/`, params).catch((error) => {
      if(error.response.status == 401){
        //alert('Your login session has expired. Please login again to proceed.')
        //store.dispatch(PURGE_AUTH);
      }
    })
  },

  update (resource, slug, params) {
    return Vue.axios.put(`${resource}/${slug}`, params).catch((error) => {
      throw new Error(`[RWV] ApiService ${error}`)
    })    
  },

  put (resource, params) {
    return Vue.axios
      .put(`${resource}/`, params).catch((error) => {        
        throw new Error(`[RWV] ApiService ${error}`)
      })
  },

  delete (resource, id) {
    return Vue.axios.post(`${resource}/`, id)
  }
}

export default ApiService

export const WeatherService = {
  query (model, params) {
    return ApiService.query(model, { queryfields: params })
  },
  get (model, id) {
    return ApiService.get(model, id)
  },
  post (model, params) {
    return ApiService.post(model, {params: params})
  },
  update (model, params) {
    return ApiService.update(model, {params: params})
  },
  delete (model, id) {
    return ApiService.delete(model, id)
  }
}